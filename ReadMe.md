# nebulous-cli #

Nebulous command line tool to help manage a Nebulous instance.

## Author ##

Andrew Chilton <andychilton@gmail.com> (https://chilts.org/)

## License ##

ISC.

(Ends)
