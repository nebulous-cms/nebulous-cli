// --------------------------------------------------------------------------------------------------------------------

// core
const https = require('https')
const fs = require('fs')
const path = require('path')

// npm
const async = require('async')
const fmt = require('fmt')
const mkdirp = require('mkdirp')
const unzipper = require('unzipper')

// --------------------------------------------------------------------------------------------------------------------

function builder(yargs) {
  yargs
    .option('theme', {
      alias: 't',
      describe: 'the theme to download and use for this website',
      default: 'base',
      demandOption: true,
      type: 'string',
    })

  return yargs
}

function handler(argv) {
  // console.log('argv:', argv)
  if ( typeof argv.theme === 'boolean' || argv.theme === '' ) {
    argv.theme = 'base'
    argv.t = 'base'
  }
  // console.log('argv:', argv)

  fmt.arrow('Creating a Nebulous CMS website')
  fmt.li(`Theme = ${argv.theme}`, true)
  fmt.spacer()

  if ( !(argv.theme.match(/^\w+$/)) ) {
    fmt.indent(`Error: invalid theme '${argv.theme}'`)
    process.exit(2)
    return
  }

  async.series(
    [
      download.bind(this, argv),
      createPackageJson.bind(this, argv),
    ],
    (err) => {
      fmt.arrow('Complete')
      fmt.spacer()
      fmt.indent('There are two remaining steps for you to do:')
      fmt.spacer()
      fmt.li('Install dependencies with `npm install`', true)
      fmt.li('Start your server with `npm start`', true)
      fmt.spacer()
      fmt.arrow('Finished')
    }
  )
}

function download(argv, done) {
  fmt.arrow('Downloading Theme')

  // For now we only support themes in gitlab.com/nebulous-themes, but this *should* be opened up.
  const themeUrl = `https://gitlab.com/nebulous-themes/${argv.theme}/-/archive/master/${argv.theme}-master.zip`
  https.get(themeUrl, res => {
    // fmt.indent('statusCode:', res.statusCode)
    fmt.spacer()

    fmt.arrow('Processing ZipFile')
    fmt.spacer()

    const prefix = `${argv.theme}-master/`
    const viewPrefix = `${argv.theme}-master/views/`
    const staticPrefix = `${argv.theme}-master/static/`
    const contentPrefix = `${argv.theme}-master/content/`

    fmt.indent('Contents:')
    res.pipe(unzipper.Parse())
      .on('entry', (entry) => {
        // if this entry is a dir, we can skip these
        if ( entry.type === 'Directory' ) {
          // fmt.indent(`path=${entry.path}`)
          const dirname = entry.path.substr(prefix.length)
          // fmt.indent(`path=${dirname}`)

          // see if this is a dir we're expecting to create
          if ( dirname.startsWith('static/') || dirname.startsWith('content/') || dirname.startsWith('views/') ) {
            mkdirp(dirname, err => {
              if (err) return done(err)
            })
          }
          else {
            entry.autodrain()
          }
          return
        }

        // see if this is a view, static, content file or not.
        if ( entry.path.startsWith(viewPrefix) ) {
          const filename = 'views/' + entry.path.substr(viewPrefix.length)
          fmt.li(`View    : ${filename}`, true)
          entry.pipe(fs.createWriteStream(filename))
        }
        else if ( entry.path.startsWith(staticPrefix) ) {
          const filename = 'static/' + entry.path.substr(staticPrefix.length)
          fmt.li(`Static  : ${filename}`, true)
          entry.pipe(fs.createWriteStream(filename))
        }
        else if ( entry.path.startsWith(contentPrefix) ) {
          const filename = 'content/' + entry.path.substr(contentPrefix.length)
          fmt.li(`Content : ${filename}`, true)
          entry.pipe(fs.createWriteStream(filename))
        }
        else {
          fmt.li(`Ignore  : ${entry.path.substr(prefix.length)}`, true)
          entry.autodrain()
        }
      })
      .on('close', () => {
        fmt.spacer()
        fmt.indent('Zipfile closed')
        fmt.spacer()
        done()
      })
    ;

  })
}

function createPackageJson(argv, done) {
  const dirname = path.parse(process.cwd()).name
  fmt.arrow('Creating package.json')

  const data = {
    name: dirname,
    version: '1.0.0',
    description: 'A Nebulous CMS website.',
    scripts: {
      start: "nebulous-server",
    },
    dependencies: {
      'nebulous-server': "^0.12.0",
    },
  }

  fs.writeFile('package.json', JSON.stringify(data, null, 2), 'utf8', err => {
    fmt.spacer()
    done(err)
  })
}

// --------------------------------------------------------------------------------------------------------------------

module.exports = {
  command: 'create',
  desc: 'create a new Nebulous CMS site',
  builder: builder,
  handler: handler,
}

// --------------------------------------------------------------------------------------------------------------------
